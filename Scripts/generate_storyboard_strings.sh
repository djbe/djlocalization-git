#!/bin/sh
# Update storyboard string files
#
# by 2013 André Pinto andredsp@gmail.com
# based on http://forums.macrumors.com/showthread.php?t=1467446

storyboardExt=".storyboard"
stringsExt=".strings"
newStringsExt=".strings.new"
oldStringsExt=".strings.old"
localeDirExt=".lproj"
baselprojName="Base.lproj"

# Find Base internationalization folders
find . -name "$baselprojName" | while read baselprojPath
do
    # Get Base project dir
    baselprojDir=$(dirname "$baselprojPath")

    # Find storyboard file full path inside base project folder
    find "$baselprojPath" -name "*$storyboardExt" | while read storyboardPath
    do
        # Get Base strings file full path
        baseStringsPath=$(echo "$storyboardPath" | sed "s/$storyboardExt/$stringsExt/")

        # Get storyboard file name and folder
        storyboardFile=$(basename "$storyboardPath")
        storyboardDir=$(dirname "$storyboardPath")

        # Create strings file only when storyboard file newer
        newer=$(find "$storyboardPath" -prune -newer "$baseStringsPath")
        [ -f "$baseStringsPath" -a -z "$newer" ] && {
            echo "$storyboardFile file not modified."
            continue
        }

        # Get New Base strings file full path and strings file name
        newBaseStringsPath=$(echo "$storyboardPath" | sed "s/$storyboardExt/$newStringsExt/")
        stringsFile=$(basename "$baseStringsPath")

        echo "Creating default $stringsFile for $storyboardFile..."
        ibtool --export-strings-file "$newBaseStringsPath" "$storyboardPath"
		if [[ $? != 0 ]]; then
			echo ":0: warning: Couldn't extract strings from '$storyboardPath'"
			continue
		fi
        iconv -f UTF-16 -t UTF-8 "$newBaseStringsPath" > "$baseStringsPath"
        rm "$newBaseStringsPath"

        # Get all locale strings folder with same parent as Base
        ls -d "$baselprojDir/"*"$localeDirExt" | while read localeStringsDir
        do
            # Skip Base strings folder
            [ "$localeStringsDir" = "$storyboardDir" ] && continue

            localeDir=$(basename "$localeStringsDir")
            localeStringsPath="$localeStringsDir/$stringsFile"

            # Just copy base strings file on first time
            if [ ! -e "$localeStringsPath" ]; then
                echo "Copying default $stringsFile for $localeDir..."
                cp "$baseStringsPath" "$localeStringsPath"
            else
                echo "Merging $stringsFile changes for $localeDir..."
                oldLocaleStringsPath=$(echo "$localeStringsPath" | sed "s/$stringsExt/$oldStringsExt/")
                cp "$localeStringsPath" "$oldLocaleStringsPath"

                # Merge baseStringsPath to localeStringsPath
                awk '
NR == FNR && /^\/\*/ {
    x=$0
    getline
    a[x]=$0
    next
}
/^\/\*/ {
    x=$0
    print
    getline
    $0=a[x]?a[x]:$0
    printf $0"\n\n"
}'  "$oldLocaleStringsPath" "$baseStringsPath" > "$localeStringsPath"

                rm "$oldLocaleStringsPath"
            fi
        done
    done
done
