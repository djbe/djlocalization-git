# Changelog

## 1.2.2 (2016-09-27)

- Better Swift 3 support.
- Better nullability annotations.

## 1.2.1 (2016-06-20)

- Added custom `DJLocalizedString` macros again.
- Added subspec for Swift support.

## 1.2.0 (2016-02-17)

Rewrote storyboard localized strings loading mechanism. Now the library loads all strings tables into memory (with exception of Localizable and InfoPlist). This is then used when needed when loading storyboard scenes.

## 1.1.1

- Forgot to commit replacement macros.
- Better mechanism to determine system language.
- Fallback to main bundle if language bundle not found.

## 1.1.0

- Store current storyboardname in viewcontrollers so we can re-set it on appear.

## 1.0.0

Initial release
