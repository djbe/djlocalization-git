//
//  ManualViewController.h
//  Example
//
//  Created by David Jennes on 28/04/15.
//  Copyright (c) 2015 davidjennes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManualViewController : UIViewController

- (IBAction)dismissManually:(UIButton *)sender;

@end
