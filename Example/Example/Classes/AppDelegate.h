//
//  AppDelegate.h
//  Test
//
//  Created by David Jennes on 14/02/15.
//  Copyright (c) 2015 dj. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

