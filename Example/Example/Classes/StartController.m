//
//  DetailViewController.m
//  Test
//
//  Created by David Jennes on 14/02/15.
//  Copyright (c) 2015 dj. All rights reserved.
//

#import "StartController.h"

#import "DJLocalization.h"

@implementation StartController

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear: animated];

	self.languageLabel.text = DJLocalizationSystem.shared.language.uppercaseString;
}

- (void)changeLanguage:(UIButton *)sender {
	NSString *language = sender.titleLabel.text.lowercaseString;
	
	
	NSLog(@"Changing language to %@", language);
	DJLocalizationSystem.shared.language = language;
	self.languageLabel.text = language.uppercaseString;

	// show alert with new language
	UIAlertController *alert = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"DJLocalization", @"Alert title")
																   message: [NSString stringWithFormat: NSLocalizedString(@"Language changed to '%@'.", @"Alert message"), language]
															preferredStyle: UIAlertControllerStyleAlert];
	[alert addAction: [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", @"Alert button")
											   style: UIAlertActionStyleDefault
											 handler: nil]];
	[self presentViewController: alert animated: YES completion: nil];
}

- (void)showManually:(UIButton *)sender {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main2" bundle: nil];
	UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier: @"manual"];

	[self presentViewController: vc animated: YES completion: nil];
}

@end
