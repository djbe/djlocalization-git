//
//  DetailViewController.h
//  Test
//
//  Created by David Jennes on 14/02/15.
//  Copyright (c) 2015 dj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartController : UIViewController

- (IBAction)changeLanguage:(UIButton *)sender;
- (IBAction)showManually:(UIButton *)sender;

@property (nonatomic, weak) IBOutlet UILabel *languageLabel;

@end

