//
//  ManualViewController.m
//  Example
//
//  Created by David Jennes on 28/04/15.
//  Copyright (c) 2015 davidjennes. All rights reserved.
//

#import "ManualViewController.h"

@implementation ManualViewController

- (void)dismissManually:(UIButton *)sender {
	[self dismissViewControllerAnimated: YES completion: nil];
}

@end
