//
//  DJLocalization.swift
//  DJLocalization
//
//  Created by David Jennes on 20/06/16.
//  Copyright (c) 2016. All rights reserved.
//

import Foundation

/**
	Returns a localized version of a string from the specified table.

	- Parameter key: The key for a string in the specified table.
	- Parameter tableName: The name of the table containing the key-value pairs.
		Also, the suffix for the strings file (a file with the .strings
		extension) to store the localized string.
	- Parameter comment: The comment to place above the key-value pair in the
		strings file.
	
	- Returns: The result of invoking `localizedStringForKey:value:table:` on the
		currently selected language bundle, passing it the specified key and tableName.
*/
public func DJLocalizedString(_ key: String, tableName: String? = nil, comment: String) -> String {
	return DJLocalizationSystem.shared.localizedString(forKey: key, value: "", table: tableName)
}
