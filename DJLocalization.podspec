Pod::Spec.new do |s|
	# info
	s.name			= 'DJLocalization'
	s.version		= '1.2.2'
	s.summary		= 'Localization system for iOS'
	s.description	= 'This framework should be used for apps that need to be able to switch the active language at runtime.'
	s.homepage		= 'https://bitbucket.org/djbe/djlocalization'
	s.license		= 'MIT'
	s.author		= { 'David Jennes' => 'david.jennes@gmail.com' }
	
	# configuration
	s.platform		= :ios, '6.0'
	s.requires_arc	= true
	
	# files
	s.frameworks	= 'Foundation', 'UIKit'
	s.source		= { :hg => 'https://bitbucket.org/djbe/djlocalization', :revision => "#{s.version}" }
	s.default_subspecs = 'Core'
	
	s.subspec 'Core' do |ss|
		ss.source_files	= 'DJLocalization/*.{h,m}'
		ss.private_header_files = 'DJLocalization/*+Private.h'
	end
	
	s.subspec 'Swift' do |ss|
		ss.platform		= :ios, '8.0'
		ss.source_files = 'DJLocalization/DJLocalization.swift'
		ss.dependency 'DJLocalization/Core'
	end
end
